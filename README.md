# spany-down-ps

#### 简介
批量文件下载器 PowerShell 版，类似于迅雷批量下载功能，且可以破解 Referer 防盗链

#### 如何使用
在文件目录，右键 spany-down-ps.ps1 选择“使用 PowerShell 运行”，如提示此系统上禁止运行脚本，可执行命令 `Set-ExecutionPolicy -ExecutionPolicy Unrestricted` 更改执行策略

也可以在 Windows PowerShell 工具，定位到脚本所在目录，通过 `.\spany-down-ps.ps1` 执行脚本

然后按照屏幕提示，输入必要参数，启动下载进程，截图演示下载某网站的美女写真集

如果你直接打开图片或者用迅雷批量下载，都将被 403 Forbidden，因为网站启用了 Referer 防盗链

![运行界面](https://images.gitee.com/uploads/images/2019/1214/083742_6e2a6822_910237.png "20191214083501.png")

另一个 C#/.NET Core 版 [spany-down-sharp](https://gitee.com/codefelix/spany-down-sharp) 采用异步下载，可以显示下载进度，效率更高，可以试试